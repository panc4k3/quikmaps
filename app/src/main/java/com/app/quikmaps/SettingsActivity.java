package com.app.quikmaps;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.app.quikmaps.fragments.AddressFragment;
import com.app.quikmaps.fragments.SettingsFragment;
import com.app.quikmaps.fragments.UserRoutesFragment;
import com.app.quikmaps.models.Constants;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        switch (getIntent().getIntExtra(Constants.SETTINGS_EXTRA_KEY, 0)) {

            case Constants.MENU_USER_PROFILE:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.settings, new SettingsFragment(new ProgressDialog(this)))
                        .commit();
                break;

            case Constants.MENU_USER_ROUTES:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.settings, new UserRoutesFragment())
                        .commit();
                break;

            case Constants.MENU_USER_ADDRESSES:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.settings, new AddressFragment())
                        .commit();
                break;

            default:
                finish();
                break;

        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}