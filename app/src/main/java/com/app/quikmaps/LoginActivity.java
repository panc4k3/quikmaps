package com.app.quikmaps;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.quikmaps.models.Address;
import com.app.quikmaps.models.Constants;
import com.app.quikmaps.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.fieldEmail)
    EditText mEmailField;
    @BindView(R.id.fieldPassword)
    EditText mPasswordField;

    ProgressDialog progressDialog;

    private FirebaseAuth mAuth;
    private  FirebaseUser mUser;
    FirebaseFirestore mFirestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        progressDialog = new ProgressDialog(this);
    }

    @OnClick(R.id.emailSignInButton)
    public void onEmailSignInClick(View view) {
        signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
    }

    @OnClick(R.id.emailCreateAccountButton)
    public void onEmailCreateAccountClick(View view) {
        createAccount(mEmailField.getText().toString(), mPasswordField.getText().toString());
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null) {
            startMainActivity();
        }
    }

    private void createAccount(String email, String password) {
        if(validateForm()) {
            showProgressDialog();

            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()) {
                                createUserFirestore();
                            } else {
                                Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_LONG).show();
                                hideProgressDialog();
                            }
                        }
                    });
        } else {
            Toast.makeText(LoginActivity.this, "Invalid Email/Password", Toast.LENGTH_LONG).show();
        }
    }

    private void signIn(String email, String password) {
        if(validateForm()) {
            showProgressDialog();

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()) {
                                mUser = mAuth.getCurrentUser();
                                queryUserExists();
                            } else {
                                Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_LONG).show();
                                hideProgressDialog();
                            }
                        }
                    });
        } else {
            Toast.makeText(LoginActivity.this, "Invalid Email/Password", Toast.LENGTH_LONG).show();
        }
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if(TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if(TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }

    private void queryUserExists() {
        if(mUser == null) {
            return;
        }

        DocumentReference uid = mFirestore.collection("users").document(mUser.getUid());

        uid
           .get()
           .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
               @Override
               public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                   String email = null;
                   if(task.isSuccessful()) {
                       DocumentSnapshot userSnapshot = task.getResult();
                       if(userSnapshot != null && userSnapshot.exists()) {
                           User user = userSnapshot.toObject(User.class);
                           if(user != null) {
                               updateLocalUserPrefs(user);
                               startMainActivity();
                           } else {
                               createUserFirestore();
                           }
                           hideProgressDialog();
                       }
                   } else {
                       Toast.makeText(LoginActivity.this, "Failed to retrieve data", Toast.LENGTH_LONG).show();
                   }
               }
           });
    }

    private void startMainActivity() {
        Intent mainActivity = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(mainActivity);
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if(progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void createUserFirestore() {
        if(mUser == null) {
            return;
        }

        User user = new User(
                mUser.getEmail(),
                true,
                Integer.parseInt(getResources().getStringArray(R.array.mode_of_transport_values)[0]),
                getResources().getStringArray(R.array.country_values)[0],
                new Address("Not set", 0.0, 0.0, Constants.HOME_ADDRESS_KEY),
                new Address("Not set", 0.0, 0.0, Constants.WORK_ADDRESS_KEY
                ));

        mFirestore.collection("users")
                .document(mUser.getUid())
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(LoginActivity.this, "Account added", Toast.LENGTH_SHORT).show();
                        updateLocalUserPrefs(user);
                        startMainActivity();
                        hideProgressDialog();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(LoginActivity.this, "Unable to create account", Toast.LENGTH_SHORT).show();
                        hideProgressDialog();
                    }
                });
    }

    private void updateLocalUserPrefs(User user) {
        SharedPreferences userPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        userPrefs.edit()
                .putString("user_email", user.getEmail())
                .putString("mode_of_transport", Integer.toString(user.getModeTransport()))
                .putString("measure_units", user.isMetric() ? "1" : "0")
                .putString("country_codes", user.getCountryCode())
                .apply();
    }
}
