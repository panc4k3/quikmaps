package com.app.quikmaps;

import android.content.Context;
import android.graphics.Bitmap;

import com.mapbox.mapboxsdk.maps.Style;

final class StyleCycle {
    private static final String[] STYLES = new String[] {
            //Style.MAPBOX_STREETS,
            //Style.OUTDOORS,
            Style.LIGHT,
            Style.DARK,
            //Style.SATELLITE_STREETS
    };

    private int index;

    void setNextStyle() {
        index++;
        if (index == STYLES.length) {
            index = 0;
        }
    }

    String getNextStyle() {
        index++;
        if (index == STYLES.length) {
            index = 0;
        }
        return getStyle();
    }

    private String getStyle() {
        return STYLES[index];
    }

    Style.Builder getStyleBuilder(String imageId, Context context, Bitmap bitmap) {
        return new Style.Builder().fromUri(STYLES[index]).withImage(imageId, bitmap);
    }
}
