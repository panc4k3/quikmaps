package com.app.quikmaps.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.app.quikmaps.R;
import com.app.quikmaps.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

public class SettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener {


    private User userData;
    private FirebaseUser mUser;
    private FirebaseFirestore mDatabase;
    private ProgressDialog progressDialog;

    public SettingsFragment(ProgressDialog progressDialog) {this.progressDialog = progressDialog;}

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);

        mDatabase = FirebaseFirestore.getInstance();
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        getUserFromFirestore();

        Preference pref = findPreference("user_email");
        pref.setOnPreferenceChangeListener(this);

        pref = findPreference("mode_of_transport");
        pref.setOnPreferenceChangeListener(this);

        pref = findPreference("measure_units");
        pref.setOnPreferenceChangeListener(this);

        pref = findPreference("country_codes");
        pref.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object o) {
        switch (preference.getKey()) {
            case "user_email":
                userData.setEmail(o.toString());
                updateUserAuth();
                break;

            case "mode_of_transport":
                userData.setModeTransport(Integer.parseInt(o.toString()));
                break;

            case "measure_units":
                userData.setMetric(o.toString().equals("1"));
                break;

            case "country_codes":
                userData.setCountryCode(o.toString());
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + preference.getKey());
        }

        updateUserDatabase();
        return true;
    }

    private void updateUserDatabase() {
        mDatabase.collection("users")
                .document(mUser.getUid())
                .set(userData, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getContext(), "Settings updated", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Unable to update settings", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void updateUserAuth() {
        mUser.updateEmail(userData.getEmail()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getContext(), "Primary email updated", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), "Unable to update email address", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getUserFromFirestore() {
        if(mUser == null) {
            return;
        }

        showProgressDialog();

        DocumentReference uid = mDatabase.collection("users").document(mUser.getUid());
        uid.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    DocumentSnapshot userSnapshot = task.getResult();
                    if(userSnapshot != null && userSnapshot.exists()) {
                        userData = userSnapshot.toObject(User.class);
                        hideProgressDialog();
                    }
                }
            }
        });
    }

    private void showProgressDialog() {
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if(progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
