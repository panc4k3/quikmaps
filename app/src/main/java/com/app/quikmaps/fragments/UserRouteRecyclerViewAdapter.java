package com.app.quikmaps.fragments;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.quikmaps.R;
import com.app.quikmaps.fragments.UserRoutesFragment.OnListFragmentInteractionListener;
import com.app.quikmaps.models.Route;

import java.util.Calendar;
import java.util.List;

public class UserRouteRecyclerViewAdapter extends RecyclerView.Adapter<UserRouteRecyclerViewAdapter.ViewHolder> {

    private final List<Route> mValues;
    private final OnListFragmentInteractionListener mListener;

    public UserRouteRecyclerViewAdapter(List<Route> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_routes, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Calendar date = Calendar.getInstance();
        date.setTime(mValues.get(position).getDate());
        String dateText = date.get(Calendar.DAY_OF_MONTH) + "/" + date.get(Calendar.MONTH) + "/" + date.get(Calendar.YEAR);
        holder.mRouteDate.setText(dateText);
        holder.mRouteName.setText(mValues.get(position).getDestPlaceName());
        holder.mRouteAddress.setText(mValues.get(position).getDestPlaceAddress());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {return mValues.size();}

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private final TextView mRouteDate;
        private final TextView mRouteName;
        private final TextView mRouteAddress;
        private Route mItem;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            mRouteDate = view.findViewById(R.id.route_date);
            mRouteName = view.findViewById(R.id.route_name);
            mRouteAddress = view.findViewById(R.id.route_address);
        }

        @Override
        @NonNull
        public String toString() {
            return super.toString() + " '" + mRouteDate.getText() + "', '" + mRouteName.getText() + "', '" + mRouteAddress.getText() + "'";
        }
    }
}
