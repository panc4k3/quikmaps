package com.app.quikmaps.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.quikmaps.R;
import com.app.quikmaps.models.Address;
import com.app.quikmaps.models.Constants;
import com.app.quikmaps.models.User;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.Arrays;

public class AddressFragment extends Fragment implements PlaceSelectionListener {

    private FirebaseUser mUser;
    private FirebaseFirestore mDatabase;
    private User userData;

    private boolean isHomeAddress = true;

    private TextView homeAddress;
    private TextView workAddress;
    private LinearLayout placesFragment;

    public AddressFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseFirestore.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address, container, false);

        homeAddress = view.findViewById(R.id.homeAddress);
        workAddress = view.findViewById(R.id.workAddress);
        placesFragment = view.findViewById(R.id.placesFragment);

        homeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHomeAddress = true;
                homeAddress.setBackgroundColor(getResources().getColor(R.color.darkNight));
                placesFragment.setVisibility(View.VISIBLE);
            }
        });

        workAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHomeAddress = false;
                workAddress.setBackgroundColor(getResources().getColor(R.color.darkNight));
                placesFragment.setVisibility(View.VISIBLE);
            }
        });
        AutocompleteSupportFragment placesFrag = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.places_frag_address);
        if(placesFrag != null) {
            placesFrag.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS));
            placesFrag.setCountry(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("country_codes", "ZA"));
            placesFrag.setOnPlaceSelectedListener(this);
        }
        getUserFromFirestore();

        return view;
    }

    @Override
    public void onPlaceSelected(@NonNull Place place) {
        if(place.getLatLng() != null) {

            if(isHomeAddress) {
                userData.setHomeAddress(new Address(place.getAddress(), place.getLatLng().latitude, place.getLatLng().longitude, Constants.HOME_ADDRESS_KEY));
                homeAddress.setText(userData.getHomeAddress().getAddressText());
                homeAddress.setBackgroundColor(getResources().getColor(R.color.quantum_white_100));
            } else {
                userData.setWorkAddress(new Address(place.getAddress(), place.getLatLng().latitude, place.getLatLng().longitude, Constants.WORK_ADDRESS_KEY));
                workAddress.setText(userData.getWorkAddress().getAddressText());
                workAddress.setBackgroundColor(getResources().getColor(R.color.quantum_white_100));
            }
            updateUserDatabase();
            placesFragment.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onError(@NonNull Status status) {

    }

    private void getUserFromFirestore() {
        if(mUser == null) {
            return;
        }

        DocumentReference uid = mDatabase.collection("users").document(mUser.getUid());
        uid.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    DocumentSnapshot userSnapshot = task.getResult();
                    if(userSnapshot != null && userSnapshot.exists()) {
                        userData = userSnapshot.toObject(User.class);
                            Address address = userData.getHomeAddress();
                            if(address != null) {
                                homeAddress.setText(address.getAddressText());
                                address = userData.getWorkAddress();
                                workAddress.setText(address.getAddressText());
                            }
                    }
                }
            }
        });
    }

    private void updateUserDatabase() {
        mDatabase.collection("users")
                .document(mUser.getUid())
                .set(userData, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getContext(), "Settings updated", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Unable to update settings", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
