package com.app.quikmaps.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.quikmaps.R;
import com.app.quikmaps.models.Route;
import com.app.quikmaps.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

public class UserRoutesFragment extends Fragment {
    private List<Route> routes = new ArrayList<>();
    private OnListFragmentInteractionListener mListener;

    private RecyclerView recyclerView;

    public UserRoutesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getUserRoutesFromFirestore();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_route, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new UserRouteRecyclerViewAdapter(routes, mListener));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Route item);
    }

    private void getUserRoutesFromFirestore() {
        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseFirestore mDatabase = FirebaseFirestore.getInstance();

        if(mUser == null) {
            return;
        }

        DocumentReference uid = mDatabase.collection("users").document(mUser.getUid());
        uid.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    DocumentSnapshot userSnapshot = task.getResult();
                    if(userSnapshot != null && userSnapshot.exists()) {
                        routes = userSnapshot.toObject(User.class).getRoutes();
                        if(routes != null) {
                            recyclerView.swapAdapter(new UserRouteRecyclerViewAdapter(routes, mListener), false);
                        }
                    }
                }
            }
        });
    }
}
