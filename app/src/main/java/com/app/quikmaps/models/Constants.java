package com.app.quikmaps.models;

public class Constants {

    public static final int MENU_SIGN_OUT = 0;
    public static final int MENU_USER_PROFILE = 1;
    public static final int MENU_USER_ROUTES = 2;
    public static final int MENU_USER_ADDRESSES = 3;

    public static final int LOCATION_REQUEST_CODE = 0;
    public static final int VIBRATE_REQUEST_CODE = 1;

    public static final String SETTINGS_EXTRA_KEY = "menu";
    public static final String HOME_ADDRESS_KEY = "home";
    public static final String WORK_ADDRESS_KEY = "work";
}
