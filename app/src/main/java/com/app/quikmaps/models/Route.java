package com.app.quikmaps.models;

import java.util.Date;

public class Route {
    private Date date;

    private double originLat;
    private double originLng;

    private double destLat;
    private double destLng;

    private String destPlaceAddress;
    private String destPlaceName;

    public Route() {}

    public Route(Date date, double originLatitude, double originLongitude, double destinationLatitude, double destinationLongitude, String destinationAddress, String destinationName) {
        this.date = date;

        originLat = originLatitude;
        originLng = originLongitude;

        destLat = destinationLatitude;
        destLng = destinationLongitude;

        destPlaceAddress = destinationAddress;
        destPlaceName = destinationName;
    }

    public Date getDate() {
        return date;
    }

    public double getOriginLat() {
        return originLat;
    }

    public double getOriginLng() {
        return originLng;
    }

    public double getDestLat() {
        return destLat;
    }

    public double getDestLng() {
        return destLng;
    }

    public String getDestPlaceAddress() {
        return  destPlaceAddress;
    }

    public String getDestPlaceName() {
        return destPlaceName;
    }
}