package com.app.quikmaps.models;

import java.util.ArrayList;
import java.util.List;

public class User {

    private String email;
    private boolean metric;
    private int modeTransport;
    private List<Route> routes;
    private String countryCode;
    private Address homeAddress;
    private Address workAddress;

    public User() {}

    public User(String email, Boolean metricUnits, int transportMode, String countryCode, Address homeAddress, Address workAddress) {
        this.email = email;
        this.metric = metricUnits;
        this.modeTransport = transportMode;
        this.countryCode = countryCode;
        routes = new ArrayList<>();
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
    }

    public String getEmail() { return email; }

    public int getModeTransport() { return modeTransport; }

    public boolean isMetric() { return metric; }

    public String getCountryCode() { return countryCode; }

    public List<Route> getRoutes() { return routes; }

    public Address getHomeAddress() { return homeAddress; }

    public Address getWorkAddress() { return workAddress; }

    public void addRoute(Route route) {
        if (routes == null) {
            routes = new ArrayList<>();
        }
        routes.add(route);
    }

    public void setEmail(String email) { this.email = email; }

    public void setMetric(boolean metric) { this.metric = metric; }

    public void setModeTransport(int mode) { this.modeTransport = mode; }

    public void setCountryCode(String code) { this.countryCode = code; }

    public void setHomeAddress(Address homeAddress) { this.homeAddress = homeAddress; }

    public void setWorkAddress(Address workAddress) { this.workAddress = workAddress; }
}
