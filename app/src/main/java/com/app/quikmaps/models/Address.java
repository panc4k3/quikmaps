package com.app.quikmaps.models;

public class Address {
    private String addressText;
    private double latitude;
    private double longitude;
    private String addressName;

    public Address() {}

    public Address(String address, double lat, double lng, String name) {
        addressText = address;
        latitude = lat;
        longitude = lng;
        addressName = name;
    }

    public String getAddressText() {
        return addressText;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
