package com.app.quikmaps;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.quikmaps.models.Constants;
import com.app.quikmaps.models.Route;
import com.app.quikmaps.models.User;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.mapbox.android.core.location.LocationEngineCallback;
import com.mapbox.android.core.location.LocationEngineResult;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, MapboxMap.OnMapLongClickListener, Callback<DirectionsResponse>, PlaceSelectionListener, OnRequestPermissionsResultCallback {

    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.routeLoadingProgressBar)
    ProgressBar routeLoading;
    @BindView(R.id.fabRemoveRoute)
    FloatingActionButton fabRemoveRoute;
    @BindView(R.id.fabStartNavigation)
    FloatingActionButton fabStartNav;
    @BindView(R.id.toolBar)
    Toolbar toolbar;

    AutocompleteSupportFragment placesFrag;
    PlacesClient placesClient;
    Place destPlace;

    MapboxMap mapboxMap;
    NavigationMapRoute navigationMapRoute;
    DirectionsRoute directionsRoute;
    StyleCycle styleCycle = new StyleCycle();

    SymbolManager markerManager;
    Symbol destinationMarker;
    LatLng destinationLngLat = null;
    LatLng originLatLng = null;

    Float INITIAL_ZOOM = 12f;

    FirebaseAuth mAuth;
    FirebaseUser mUser;
    FirebaseFirestore mDatabase;

    User userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseFirestore.getInstance();
        getUserFromFirestore();

        Places.initialize(getApplicationContext(), getString(R.string.google_places_key));
        placesClient = Places.createClient(this);

        placesFrag = (AutocompleteSupportFragment)getSupportFragmentManager().findFragmentById(R.id.places_frag);
        placesFrag.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS));
        placesFrag.setCountry(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("country_codes", "ZA"));
        placesFrag.setOnPlaceSelectedListener(this);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        setupToolBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.user_sign_out:
                userSignOut();
                return true;

            case  R.id.user_settings:
                userSettings(Constants.MENU_USER_PROFILE);
                return true;

            case R.id.user_route_history:
                userSettings(Constants.MENU_USER_ROUTES);
                return true;

            case R.id.user_addresses:
                userSettings(Constants.MENU_USER_ADDRESSES);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.fabStyles)
    public void onStyleFabClick() {
        if(mapboxMap != null) {
            styleCycle.setNextStyle();
            mapboxMap.setStyle(styleCycle.getStyleBuilder("marker-dest", getApplicationContext(), BitmapFactory.decodeResource(getResources(), R.drawable.mapbox_marker_icon_default)));
        }
    }

    @OnClick(R.id.fabRemoveRoute)
    public void onRemoveRouteClick(View fabRemoveRoute) {
        removeRouteAndMarkers();
        fabRemoveRoute.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.fabStartNavigation)
    public void onStartNavigationClick(View view) {
        CameraPosition initialPosition = new CameraPosition.Builder()
                .target(originLatLng)
                .zoom(INITIAL_ZOOM)
                .build();

        NavigationLauncherOptions options = NavigationLauncherOptions.builder()
                .directionsRoute(directionsRoute)
                .initialMapCameraPosition(initialPosition)
                .shouldSimulateRoute(false)
                .darkThemeResId(R.style.NavigationViewDark)
                .lightThemeResId(R.style.NavigationViewLight)
                .build();

        int legs = directionsRoute.legs().size() - 1;
        int steps = directionsRoute.legs().get(legs).steps().size() - 1;
        String dest = directionsRoute.legs().get(legs).steps().get(steps).name();
        dest = dest.equals("") ? "Unavaliable" : dest;

        String placeAddress = destPlace!= null ? destPlace.getAddress() : dest;
        String placeName = destPlace != null ? destPlace.getName() : "Unavailable";

        userData.addRoute(new Route(
                new Date(),
                originLatLng.getLatitude(),
                originLatLng.getLongitude(),
                destinationLngLat.getLatitude(),
                destinationLngLat.getLongitude(),
                placeAddress,
                placeName
        ));
        updateUserDatabase();

        NavigationLauncher.startNavigation(this, options);
    }

    @Override
    public boolean onMapLongClick(@NonNull LatLng point) {
        findRouteFromLocation(point);
        return true;
    }

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        this.mapboxMap.addOnMapLongClickListener(this);
        Snackbar.make(mapView, "Long press to select route", Snackbar.LENGTH_SHORT).show();

        mapboxMap.setStyle(styleCycle.getStyleBuilder("marker-dest", getApplicationContext(), BitmapFactory.decodeResource(getResources(), R.drawable.mapbox_marker_icon_default)), style -> {
            initializeLocationComponent(this.mapboxMap);
            navigationMapRoute = new NavigationMapRoute(null, mapView, this.mapboxMap);

            if(this.mapboxMap.getStyle() != null) {
                markerManager = new SymbolManager(mapView, this.mapboxMap, style);
            }
        });
    }

    @Override
    public void onResponse(@NonNull Call<DirectionsResponse> call, @NonNull Response<DirectionsResponse> response) {
        if(response.isSuccessful() && response.body() != null && !response.body().routes().isEmpty()) {
            List<DirectionsRoute> routes = response.body().routes();
            navigationMapRoute.addRoutes(routes);
            directionsRoute = response.body().routes().get(0);
            routeLoading.setVisibility(View.INVISIBLE);
            fabRemoveRoute.show();
            fabStartNav.show();
        }
    }

    @Override
    public void onFailure(@NonNull Call<DirectionsResponse> call, @NonNull Throwable t) {
        Timber.e(t);
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
        if(navigationMapRoute != null) {
            navigationMapRoute.onStart();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        if (navigationMapRoute != null) {
            navigationMapRoute.onStop();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        markerManager.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.LOCATION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initializeLocationComponent(this.mapboxMap);
                } else {
                    finish();
                }
                break;

            case Constants.VIBRATE_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    vibrate();
                } else {
                    finish();
                }
                break;

            default:
                finish();
                break;
        }
    }

    private void initializeLocationComponent(MapboxMap mapboxMap) {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
           ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            LocationComponent locationComponent = mapboxMap.getLocationComponent();
            LocationComponentActivationOptions locationComponentActivationOptions = LocationComponentActivationOptions.builder(this, mapboxMap.getStyle()).build();
            locationComponent.activateLocationComponent(locationComponentActivationOptions);
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setRenderMode(RenderMode.COMPASS);
            locationComponent.setCameraMode(CameraMode.TRACKING);
            locationComponent.zoomWhileTracking(10d);
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_REQUEST_CODE);
        }

    }

    private void findRouteFromLocation(@NonNull LatLng point) {
        vibrate();
        if(destinationMarker != null) {
            markerManager.delete(destinationMarker);
        }
        if(markerManager != null) {
            destinationMarker = markerManager.create(new SymbolOptions().withLatLng(point).withIconImage("marker-dest"));
        }

        destinationLngLat = point;
        mapboxMap.getLocationComponent().getLocationEngine().getLastLocation(new LocationEngineCallback<LocationEngineResult>() {
            @Override
            public void onSuccess(LocationEngineResult result) {
                if(result.getLastLocation() != null) {
                    originLatLng = new LatLng(result.getLastLocation().getLatitude(), result.getLastLocation().getLongitude());
                    Point originPoint = Point.fromLngLat(result.getLastLocation().getLongitude(), result.getLastLocation().getLatitude());
                    Point destinationPoint = Point.fromLngLat(
                            destinationLngLat.getLongitude(),
                            destinationLngLat.getLatitude());
                    Snackbar.make(mapView, "Destination selected", Snackbar.LENGTH_SHORT).show();
                    findRoute(originPoint, destinationPoint);
                    routeLoading.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(getApplicationContext(),"Unable to get location", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void vibrate() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.VIBRATE) == PackageManager.PERMISSION_GRANTED) {
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (vibrator == null) {
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                vibrator.vibrate(100);
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.VIBRATE}, Constants.VIBRATE_REQUEST_CODE);
        }
    }

    private void removeRouteAndMarkers() {
        markerManager.delete(destinationMarker);
        navigationMapRoute.updateRouteVisibilityTo(false);
        fabStartNav.hide();
    }

    public void findRoute(Point origin, Point destination) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String modeOfTransport = prefs.getString("mode_of_transport", "0");
        String metric = prefs.getString("measure_units", "1");

        NavigationRoute.builder(this)
                .accessToken(getString(R.string.mapbox_access_token))
                .origin(origin)
                .destination(destination)
                .voiceUnits(metric.equals("1") ? DirectionsCriteria.METRIC : DirectionsCriteria.IMPERIAL)
                .profile(modeOfTransport.equals("0") ? DirectionsCriteria.PROFILE_DRIVING_TRAFFIC : modeOfTransport.equals("1") ? DirectionsCriteria.PROFILE_WALKING : DirectionsCriteria.PROFILE_CYCLING)
                .alternatives(true)
                .build()
                .getRoute(this);
    }

    @Override
    public void onPlaceSelected(@NonNull Place place) {
        destPlace = place;
        if(place.getLatLng() != null) {
            findRouteFromLocation(new LatLng(place.getLatLng().latitude, place.getLatLng().longitude));
        }
    }

    @Override
    public void onError(@NonNull Status status) {

    }

    private void setupToolBar() {
        if(toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    public void userSignOut() {
        if(mAuth != null) {
            mAuth.signOut();
            finish();
        }
    }

    public void userSettings(int menu) {
        Intent settingsActivity = new Intent(this, SettingsActivity.class);
        settingsActivity.putExtra(Constants.SETTINGS_EXTRA_KEY, menu);
        startActivity(settingsActivity);
    }

    private void getUserFromFirestore() {
        if(mUser == null) {
            return;
        }

        DocumentReference uid = mDatabase.collection("users").document(mUser.getUid());
        uid.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()) {
                            DocumentSnapshot userSnapshot = task.getResult();
                            if(userSnapshot != null && userSnapshot.exists()) {
                                userData = userSnapshot.toObject(User.class);
                            }
                        }
                    }
                });
    }

    private void updateUserDatabase() {
        mDatabase.collection("users")
                .document(mUser.getUid())
                .set(userData, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(MainActivity.this, "Settings updated", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, "Unable to update settings", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
